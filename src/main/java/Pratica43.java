
import utfpr.ct.dainf.if62c.pratica.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author montaguti
 */
public class Pratica43 {
    public static void main(String[] args) {
        Quadrado quadrado = new Quadrado(5);
        System.out.println("Area do quadrado: " + quadrado.getArea());
        System.out.println("Perimetro do quadrado: " + quadrado.getPerimetro());
        System.out.println("Lado menor do quadrado: " + quadrado.getLadoMenor());
        
        TrianguloEquilatero triangulo = new TrianguloEquilatero(5);
        System.out.println("Area do triangulo: " + triangulo.getArea());
        System.out.println("Perimetro do triangulo: " + triangulo.getPerimetro());
        System.out.println("Lado menor do triangulo: " + triangulo.getLadoMenor());
        
        Retangulo retangulo = new Retangulo(5, 20);
        System.out.println("Area do retangulo: " + retangulo.getArea());
        System.out.println("Perimetro do retangulo: " + retangulo.getPerimetro());
        System.out.println("Lado menor do retangulo: " + retangulo.getLadoMenor());
        
    }
}

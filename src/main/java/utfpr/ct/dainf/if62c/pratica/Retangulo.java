/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author montaguti
 */
public class Retangulo extends Quadrilatero implements FiguraComLados {

    public Retangulo() {
        super();
    }

    public Retangulo(double base, double altura) {
        super(base, altura);
    }

    @Override
    public double getArea() {
        return getBase() * getAltura();
    }

    @Override
    public double getPerimetro() {
        return (getBase() + getAltura()) * 2;
    }

    @Override
    public double getLadoMaior() {
        return this.getAltura() > this.getBase() ? this.getAltura() : this.getBase();
    }

    @Override
    public double getLadoMenor() {
        return this.getAltura() < this.getBase() ? this.getAltura() : this.getBase();
    }
}
